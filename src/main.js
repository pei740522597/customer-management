// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
// require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

// Uncomment the following lines if you need IE11/Edge support
require(`quasar/dist/quasar.ie`)
require(`quasar/dist/quasar.ie.${__THEME}.css`)

import Vue from 'vue'
import Quasar, * as All from 'quasar'
import router from './router'
import store from './store'
import axios from 'axios'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(Quasar, {
  components: All,
  directives: All
}) // Install Quasar Framework
Vue.use(VueMomentJS, moment)
Vue.use(VueResource)

if (__THEME === 'mat') {
  require('quasar-extras/roboto-font')
}
import 'quasar-extras/material-icons'
// import 'quasar-extras/ionicons'
// import 'quasar-extras/fontawesome'
// import 'quasar-extras/animate'

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  store.commit('startLoading')
  return config
}, function (error) {
  store.commit('stopLoading')
  return Promise.reject(error)
})

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  store.commit('stopLoading')
  if (response.data) {
    if (!response.data.success && response.data.msg === 'Unauthorized') {
      if (PROD) {
        window.location.href = 'quote/login?redirect=management'
      }
    }
  }
  return response
}, function (error) {
  store.commit('stopLoading')
  return Promise.reject(error)
})

Quasar.start(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#q-app',
    router,
    store,
    render: h => h(require('./App').default)
  })
})
