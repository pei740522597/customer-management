import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    loading: 0,
    breadcrumbs: '',
    user: null,
    suppliers: [],
    cost_categories: [],
    vehicles: [],
    reimbursements: {
      items: [],
      total: 0
    },
    submitedReimbursements: {
      items: [],
      total: 0
    },
    odometers: {
      items: [],
      total: 0
    }
  },
  mutations: {
    startLoading (state) {
      state.loading++
    },
    stopLoading (state) {
      state.loading--
    },
    updateBreadcrumbs (state, pageRoute) {
      state.breadcrumbs = pageRoute
    },
    setUser (state, user) {
      state.user = user
    },
    setSuppliers (state, suppliers) {
      state.suppliers = suppliers
    },
    setCostCategories (state, costCategories) {
      state.cost_categories = costCategories
    },
    setVehicles (state, vehicles) {
      state.vehicles = vehicles
    },
    setReimbursements (state, reimbursement) {
      state.reimbursements.items = reimbursement.items
      state.reimbursements.total = reimbursement.count
    },
    setSubmitedReimbursements (state, submitedReimbursement) {
      state.submitedReimbursements.items = submitedReimbursement.items
      state.submitedReimbursements.total = submitedReimbursement.count
    },
    setOdometers (state, odometers) {
      state.odometers.items = odometers.items
      state.odometers.total = odometers.count
    }
  },
  actions: {
    // Load all vehicles
    loadVehicles: function (context) {
      if (context.state.vehicles.length === 0) {
        axios.post('/api/customer/vehicles')
          .then(response => {
            let data = response.data
            if (data.success) {
              let vehicles = data.vehicles

              vehicles.forEach((vehicle) => {
                vehicle.label = vehicle.rego_id + ' - ' + vehicle.desc
                vehicle.value = vehicle.vehicle_id
                vehicle.desc = vehicle.rego_id + '-' + vehicle.desc
              })

              if (vehicles.length > 1) {
                vehicles.unshift({
                  'value': 'all',
                  'label': 'All'
                })
              }
              context.commit('setVehicles', vehicles)
            }
          })
      }
    },
    // Load Cost Categories
    loadCostCategories (context) {
      if (context.state.cost_categories.length === 0) {
        axios.post('/api/customer-reimbursement/reimbursement-cost-category')
          .then(response => {
            let data = response.data
            if (data.success) {
              let costCodes = []
              data.cost_codes.forEach((costCode) => {
                costCode.label = costCode.description
                costCode.value = costCode.cost_code_id
                costCodes.push(costCode)
              })
              context.commit('setCostCategories', costCodes)
            }
          })
      }
    },
    // Load Suppliers
    loadSuppliers (context) {
      axios.post('/api/supplier/customer-supplier-list', {type: 'Servicing, Repairs & Maintenance'}).then(response => {
        let data = response.data
        if (data.success) {
          let suppliers = []
          data.suppliers.forEach((supplier) => {
            suppliers.push({
              label: supplier,
              value: supplier
            })
          })
          context.commit('setSuppliers', suppliers)
        }
      })
    },
    // Load login user information
    loadUser (context) {
      if (!context.state.user) {
        axios.post('/api/auth/user')
          .then(response => {
            if (response.data.success) {
              context.commit('setUser', response.data.user)
            }
          }).catch(error => {
            console.error(error)
          })
      }
    },
    // Set header breadcrumbs information
    setBreadcrumbs (context, breadcrumbs) {
      context.commit('updateBreadcrumbs', breadcrumbs)
    },
    // Load reimbursements trigger
    loadReimbursements (context, params) {
      if (params.type === 'STATUS_PENDING') {
        if (params.append) {
          params.offset = context.state.reimbursements.items.length
        }

        axios.post('/api/customer-reimbursement/customer-reimbursement', params).then(response => {
          let data = response.data
          let reimbursements = data.reimbursements
          reimbursements.forEach((reimbursement) => {
            reimbursement.vehicle.desc = reimbursement.vehicle.rego_id + ' - ' + reimbursement.vehicle.desc
          })
          if (!params.append) {
            context.state.reimbursements.items = []
          }
          data.reimbursements.forEach((reimbursement) => {
            context.state.reimbursements.items.push(reimbursement)
          })
          context.commit('setReimbursements', {items: context.state.reimbursements.items, count: data.totalCount})
        })
      }

      if (params.type === 'STATUS_SUBMITTED') {
        if (params.append) {
          params.offset = context.state.submitedReimbursements.items.length
        }
        Vue.http.post('/customer-reimbursement/customer-reimbursement', params).then(response => {
          let data = response.json()
          let reimbursements = data.reimbursements

          reimbursements.forEach((reimbursement) => {
            reimbursement.vehicle.desc = reimbursement.vehicle.rego_id + ' - ' + reimbursement.vehicle.desc
          })

          if (!params.append) {
            context.state.submitedReimbursements.items = []
          }
          data.reimbursements.forEach((reimbursement) => {
            context.state.submitedReimbursements.items.push(reimbursement)
          })

          context.commit('setSubmitedReimbursements', {
            items: context.state.submitedReimbursements.items,
            count: data.totalCount
          })
        })
      }
    },
    loadOdometers (context, params) {
      if (params.append) {
        params.offset = context.state.odometers.items.length
      }
      Vue.http.post('/customer-odometer/customer-odometer-list', params).then(response => {
        let data = response.json()
        if (data.success) {
          data.odometers.forEach((odometer) => {
            odometer.vehicle.desc = odometer.vehicle.rego_id + ' - ' + odometer.vehicle.desc
          })
          if (!params.append) {
            context.state.odometers.items = []
          }
          data.odometers.forEach((odometer) => {
            context.state.odometers.items.push(odometer)
          })
          context.commit('setOdometers', {
            items: context.state.odometers.items,
            count: data.totalCount
          })
        }
      })
    }
  }
})

export default store
