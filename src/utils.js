function numberFormat (num) {
  num = num.replace(/,/g, '')
  if (isNaN(parseFloat(num))) {
    return ''
  }
  let str = parseFloat(num).toFixed(2).split('.')
  let integer = str[0]
  let decimal = str[1]
  return parseInt(integer).toLocaleString() + '.' + decimal
}

export { numberFormat }
